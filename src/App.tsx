import React from 'react';

import { Provider } from 'react-redux';
import { store } from './store';

import { Home } from './components/pages/Home/home';

// import './styles/App.less';
import 'antd/dist/antd.css';

export function App() {
  return (
    <Provider store={store}>
          <Home/>
    </Provider>
  );
}
