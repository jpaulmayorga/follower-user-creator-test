export interface User {
    _id: string;
    created_at: string;
    is_creator: boolean;
    name: string;
    username: string;
    email: string;
    avatar: string;
}

export interface GetUser {
    user: User | null;
}

export interface MutationUpdateUserVariables {
    input: {
        username?: string;
        name?: string;
        about?: string;
        cover?: string;
        avatar?: string;
        is_creator?: boolean;
        price?: number;
        max_followers_per_day?: number;
    };
}
