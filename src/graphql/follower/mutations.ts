export const MUTATION_UNFOLLOW = `
    mutation UnFollow($creator_id: String ){
        success: deleteFollow(creator_id: $creator_id)
    }
`;
export const MUTATION_FOLLOW = `
    mutation createFollow($creator_id: String, $method_subscription: String, $id_subscription: String ){
        success: createFollow(creator_id: $creator_id, method_subscription: $method_subscription, id_subscription: $id_subscription)
    }
`;
export const MUTATION_FOLLOW_ADMIN = `
    mutation createFollowAdmin($user_id: String, $creator_id: String, $method_subscription: String, $id_subscription: String, $duration: Float ){
        success: createFollowAdmin(user_id: $user_id, creator_id: $creator_id, method_subscription: $method_subscription, id_subscription: $id_subscription, duration: $duration)
    }
`;
