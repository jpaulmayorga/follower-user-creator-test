export interface Follow {
  _id: string;
  username: string;
  name: string;
  avatar: string;
}

export interface GetFollowing {
  following: Follow[];
}
export interface GetFollowingVariables {
  user_id: string;
}

export interface GetTodayFollowersCount {
  count: number;
}
export interface GetTodayFollowersCountVariables {
  creator_id: string;
}
export interface MutationFollow {
  success: boolean;
}
export interface MutationUnFollowVariables {
  creator_id: string;
}

export interface MutationFollowVariables {
  creator_id: string;
  method_subscription: string;
  id_subscription?: string;
}

export interface MutationFollwerAdmin {
  success: boolean;
}

export interface MUtatioFollowAdminVariables {
  user_id: string;
  creator_id: string;
  method_subscription: string;
  id_subscription: string;
  duration: number;
}
