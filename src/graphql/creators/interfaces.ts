export interface Creator {
    _id: string;
    auth_id: string;
    username: string;
    name: string;
    about: string;
    email: string;
    cover: string;
    avatar: string;
    created_at: string;
    is_creator: boolean;
    price: number;
    max_followers_per_day: number;
    is_followed_by_me: boolean;
    followers_count: number;
    posts_count: number;
    is_locked?: boolean;
}

export interface GetCreator {
    creator: Creator | null;
}

export interface GetCreatorVariables {
    username: string;
}

export interface GetCreatorUserVariables {
    email: string;
}
