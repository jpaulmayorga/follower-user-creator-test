export const GET_CREATOR_QUERY = `
    query GetCreator($username: String!) {
        creator: getUser(username: $username) {
        _id
        auth_id
        username
        name
        about
        email
        cover
        avatar
        created_at
        is_creator
        price
        max_followers_per_day
        is_followed_by_me
        followers_count
        posts_count
        }
    }
`;

export const GET_CREATOR_QUERY_USER = `
    query GetCreator($email: String!) {
        creator: getUser(email: $email) {
            _id
            email
            name
            avatar
        }
    }
`;
