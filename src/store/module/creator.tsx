import { Creator } from '../../graphql/creators';
import { typedAction } from '../helpers';

type CreatorState = Creator | null;
const initialState: CreatorState = null;

export const setCreator = (payload: CreatorState) => {
    return typedAction('setCreator', payload);
};

export const setCreatorLock = (payload: boolean) => {
    return typedAction('setCreatorLock', payload);
};

type Actions = ReturnType<typeof setCreator | typeof setCreatorLock>;

export function creatorReducer(state: CreatorState = initialState, action: Actions): CreatorState {
    switch (action.type) {
        case 'setCreator':
            return action.payload;
        case 'setCreatorLock':
            const isLocked = { is_locked: action.payload };
                return { ...state!, ...isLocked };
        default:
            return state;
    }
}
