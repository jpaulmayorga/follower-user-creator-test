import React, { useState, useEffect } from "react";
import {
  Form,
  Input,
  Button,
  Row,
  Col,
  Card,
  Avatar,
  Select,
  notification,
} from "antd";
import { fetchGraphql } from "../../../graphql";
import { Creator } from "../../../graphql/creators/interfaces";
import { User } from "../../../graphql/users/interfaces";
import {
  MUtatioFollowAdminVariables,
  MutationFollwerAdmin,
  MUTATION_FOLLOW_ADMIN,
} from "../../../graphql/follower";

import {
  GetCreator,
  GET_CREATOR_QUERY,
  GET_CREATOR_QUERY_USER,
  GetCreatorVariables,
  GetCreatorUserVariables,
} from "../../../graphql/creators";
import "./home.less";

export function Home() {
  const { Meta } = Card;

  const [form] = Form.useForm();

  const [duration, setDuration] = useState<number>(0);
  const [creator, setCreator] = useState<Creator | null>(null);
  const [user, setUser] = useState<User | null>(null);

  const [loadingCreator, setLoadingCreator] = useState<boolean>(false);
  const [loadingUser, setLoadingUser] = useState<boolean>(false);

  const [canSubmit, setCanSubmit] = useState<boolean>(false);

  useEffect(() => {
    if (creator?._id && user?._id && duration) {
      setCanSubmit(true);
    }
  }, [creator, user, duration]);

  const searchCreador = async (event: React.ChangeEvent<HTMLInputElement>) => {
    setLoadingCreator(true);
    const { creator } = await fetchGraphql<GetCreator, GetCreatorVariables>({
      query: GET_CREATOR_QUERY,
      variables: { username: event.target.value },
    });
    setCreator(creator);
    setLoadingCreator(false);
  };

  const searchUser = async (event: React.ChangeEvent<HTMLInputElement>) => {
    setLoadingUser(true);
    const { creator } = await fetchGraphql<GetCreator, GetCreatorUserVariables>(
      {
        query: GET_CREATOR_QUERY_USER,
        variables: { email: event.target.value },
      }
    );
    setUser(creator);
    setLoadingUser(false);
  };

  const onFinish = async () => {
    const { success } = await fetchGraphql<
      MutationFollwerAdmin,
      MUtatioFollowAdminVariables
    >({
      query: MUTATION_FOLLOW_ADMIN,
      variables: {
        user_id: user?._id!,
        creator_id: creator?._id!,
        method_subscription: "byplatform",
        id_subscription: "none",
        duration: duration,
      },
    });

    if (success) {
      console.log("corrio perfecto");
      notification.open({
        message: `Se creo el follower correctamente`,
      });
    } else {
      console.log("fallo");
      notification.open({
        message: `Algo falló`,
      });
    }
  };

  return (
    <>
      <Row justify="center" className="row-home">
        <Col xs={24}>
          <h2>Crear Relacion creator - user</h2>
        </Col>
        <Col xs={20} md={16}>
          <Form form={form} onFinish={onFinish}>
            <Form.Item name="creator" label="Creator">
              <Input placeholder="username" onChange={searchCreador} />
            </Form.Item>

            <Form.Item name="user" label="User">
              <Input placeholder="email" onChange={searchUser} />
            </Form.Item>

            <Form.Item name="tiempo" label="tiempo de duracion">
              <Select
                placeholder="selecionar tiempo"
                onChange={(e) => setDuration(Number(e))}
              >
                <Select.Option value="seleccionar-tiempo" disabled>
                  seleccionar tiempo
                </Select.Option>
                <Select.Option value="1">1 mes</Select.Option>
                <Select.Option value="2">2 meses</Select.Option>
                <Select.Option value="3">3 meses</Select.Option>
                <Select.Option value="0">infinito</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" disabled={!canSubmit}>
                Crear Relacion
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>

      <Row justify="center" className="row-card">
        <Col>
          <h2>Creador</h2>
          <Card style={{ width: 300 }}>
            {loadingCreator ? (
              "Cargando creador"
            ) : (
              <Meta
                avatar={<Avatar src={creator?.avatar} />}
                title={creator?.username}
                description={creator?.about}
              />
            )}
          </Card>
          ,
        </Col>
        <Col>
          <h2>Usuario</h2>
          <Card style={{ width: 300 }}>
            {loadingUser ? (
              "Cargando user.."
            ) : (
              <Meta
                avatar={<Avatar src={user?.avatar} />}
                title={user?.name}
                description={user?.email}
              />
            )}
          </Card>
          ,
        </Col>
      </Row>
    </>
  );
}
