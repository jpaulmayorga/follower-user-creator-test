# MiPriv

## Getting started

### Requirements

Make sure the following tools are installed in your system:

- [Node v12.X.X](https://nodejs.org/en/download/)
- [Yarn v1.21.X](https://yarnpkg.com/en/docs/install)

### Installation

Clone the GitHub repository and use `yarn` to install the dependencies.

```
$ git clone https://github.com/<username>/mipriv.git
$ cd mipriv
$ yarn install
```

Copy `.env.sample` into `.env`

## Development

### Local

To start developing simply run:

```
$ yarn dev
```

[NextJS](https://nextjs.org) will compile and start the application, open a browser and visit localhost:3000. You can start making some changes and each one will trigger an update to the website.

### Build

To create a production build simply run:

```
$ yarn build
```

NextJS will perform an optimized production build for your site.

To serve the production build locally run:

```
$ yarn start
```

Gatsby starts a local server for testing your build site at localhost:3000.

## Deployment

We recommend using [Github](https://github.com) + [Vercel](https://vercel.com).

1. Create a repository on GitHub
2. Create an account in Vercel
3. Create a Vercel project using the created repository
4. Add the [environment variables](https://vercel.com/blog/environment-variables-ui) for production, preview and deployment. For preview and deploymeent you can use the same values located in `.env.sample`, just change the `STRIPE_DOMAIN_URL` to the domain where you'll do the tests (could be `dev` branch)

## Main technologies used

1. [NextJS](https://nextjs.org): A React framework for server-rendered universal JavaScript webapps, built on top of React, Webpack and Babel.
2. [Tailwind CSS](https://tailwindcss.com): Tailwind CSS is a highly customizable, low-level CSS framework that gives you all of the building blocks you need to build bespoke designs without any annoying opinionated styles you have to fight to override.
3. [Vercel](https://vercel.com): A cloud platform for static sites and Serverless Functions that fits perfectly with your workflow. It enables developers to host Jamstack websites and web services that deploy instantly, scale automatically, and requires no supervision, all with no configuration.

## 3rd party services used

### [Airtable](https://airtable.com)

For data storing purposes. We have two bases for dev and prod. Both should have the same tables and columns in any time.

We have the following tables:

1. **users**: store creators and users data.
2. **posts**: store posts data. A new record is created when a creator adds content via their dashboard. We have text, image and video kinds.
3. **followers**: store who users have access to creators. A new record should be inserted after a given user has paid for access.

### [Auth0](https://auth0.com)

For authentication and authorization purposes. We have two tenants for dev and prod. Each tenant contains a [rule](https://auth0.com/docs/rules) that inserts a new record on Airtable on every new Sign up, to have in our own database the users created by Auth0.

### [Cloudinary](https://cloudinary.com)

For media (images and videos) storing purposes. We have two folders (based on presets) for dev and prod. Each image has a `userId` metadata to facilitate filtering by user. We're using the [Upload Widget](https://cloudinary.com/documentation/upload_widget) to speed up the development but Cloudinary has support to develop in-house interactive media upload capabilities.

### [Stripe](https://stripe.com/en-mx)

For [payments via OXXO](https://github.com/stripe-samples/oxxo-payment) support. To be able to use stripe, a backend is needed for security reasons. In our case, we're using Vercel functions to speed up the developement and all we need is the function found in `create-payment-intent.ts`.

## Directory layout

```
.
├── public/                 <-- Static assets
├── src/                    <-- Application components and source code
```

Inside `src` folder we have:

- **components:** Reusable UI components used in the main pages
- **lib:** Helpers to access our API and Auth0 logic
- **pages:** Main pages the users see on the web application
- **styles:** CSS styles

### Pages

We have the following pages:

1. **index**: root page.
2. **auth**: Page that Auth0 redirects after a login/signup. Here we verify the auth status using `auth.tsx`.
3. **[username]**: Template page for creators content. Intended to be viewed by users. Here we verify if the username exists, if the user is logged in, if the user has permissions to see the content and display an oxxo payment form and voucher for the user to pay.
4. **users/index**: Intended to place information for users.
5. **users/dashboard**: Main page of logged in user. Here we display a creators search and a list of creators the user has access to.
6. **creators/index**: Intended to place information for creators.
7. **creators/dashboard**: Main page of logged in creator. Here we display a form to finish signup asking for username and paypal account, display the contents their made and a modal to add more content.

### API

We have two backend functions (inside `src/pages/api`):

1. **create-checkout-session**: Enables to use stripe's [Checkout](https://stripe.com/docs/payments/checkout) to accept card payments.
2. **create-payment-intent**: Enables to accept [payments via oxxo](https://stripe.com/docs/payments/oxxo) using stripe.

## Next steps

1. Enhance error handling on every API interaction. Now only the happy paths are covered but better messaging and UI could be use.
2. Replace Airtable with SQL database. Airtable it's not a good choice on scale given their API calls limitations. Migration should be done following the steps: 1) create sql schema of database with the 3 tables; 2) setup database with provider of choice; 3) creating backend functions to access database and serve our frontend; 4) change `src/lib/api.ts` to point to our backend and process the response data (if we mantain the same return responses we shouldn't change any logic on the UI components)
3. Add analytics using Google Analytics or Amplitude for specific events.
